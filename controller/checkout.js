const Router = require('express').Router;
const router = new Router();
const Security = require('../lib/security');
const reqPay = require('../reqPay');
const pagarme = require('pagarme');


router.route('/').post((req, res) => {
  const sess = req.session;
  const cart = (typeof sess.cart !== 'undefined') ? sess.cart : false;
  if (Security.isValidNonce(req.body.nonce, req) && sess.cart) {
    reqPay.request.amount = cart.totals + reqPay.request.shipping.fee;
    reqPay.request.items = cart.items;
    cart.items.forEach((obj) => {
      obj.id = obj._id;
      obj.title = obj.name;
      obj.unit_price = obj.value;
      obj.quantity = obj.qty;
      obj.tangible = true;
      delete obj._id;
      delete obj.name;
      delete obj.qty;
      delete obj.value;
      delete obj.imageUrl;
      delete obj.factor;
    });
    pagarme.client.connect({
      api_key: reqPay.apiKey
    })
    .then(client => client.transactions.create(reqPay.request))
    .then(transaction => res.json(`Compra efetuada no valor de ${transaction.amount} acrescentado o Frete de ${transaction.shipping.fee}`))
    .catch(err => res.json(err));

  } else {
    res.json('Deu errado');
  }
});

module.exports = router;

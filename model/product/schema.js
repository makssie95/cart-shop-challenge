const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const productSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String
  },
  value: {
    type: Number,
    required: true
  },
  factor: {
    type: String,
    required: true,
    enum: ['A', 'B', 'C']
  }
});

module.exports = mongoose.model('Products', productSchema);

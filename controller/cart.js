const Router = require('express').Router;
const router = new Router();
const Security = require('../lib/security');
const Products = require('../model/product/schema');
const Cart = require('../lib/cart');

router.route('/').get((req, res) => {
  const sess = req.session;
  const cart = (typeof sess.cart !== 'undefined') ? sess.cart : false;
  res.json({
    pageTitle: 'Cart',
    cart,
    nonce: Security.md5(req.sessionID + req.headers['user-agent'])
  });
});

router.route('/').post((req, res) => {
  const qty = parseInt(req.body.qty, 10);
  const id = req.body._id;
  if (!req.session.cart) {
    req.session.cart = {
      items: [],
      totals: 0.00,
      formattedTotals: ''
    };
  }
  if (qty > 0 && Security.isValidNonce(req.body.nonce, req)) {
    Products.findOne({
      _id: id
    }).then((prod) => {
      const cart = (req.session.cart) ? req.session.cart : null;
      Cart.addToCart(prod, qty, cart);
      res.status(200).send('Produto Inserido com sucesso');
    });
  } else {
    res.redirect('/');
  }
});

router.route('/empty/:nonce').delete((req, res) => {
  if (Security.isValidNonce(req.params.nonce, req)) {
    Cart.emptyCart(req);
    res.redirect('/cart');
  } else {
    res.redirect('/products');
  }
});

router.route('/remove/:id/:nonce').delete((req, res) => {
  const id = req.params.id;
  if (Security.isValidNonce(req.params.nonce, req)) {
    Cart.removeFromCart(id, req.session.cart);
    res.redirect('/cart');
  } else {
    res.redirect('/products');
  }
});

router.route('/update').post((req, res) => {
  const ids = req.body._id;
  const qtys = req.body.qty;
  if (Security.isValidNonce(req.body.nonce, req)) {
    const cart = (req.session.cart) ? req.session.cart : null;
    const i = (!Array.isArray(ids)) ? [ids] : ids;
    const q = (!Array.isArray(qtys)) ? [qtys] : qtys;
    Cart.updateCart(i, q, cart);
    res.redirect('/cart');
  } else {
    res.redirect('/products');
  }
});

module.exports = router;

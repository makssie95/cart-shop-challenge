const express = require('express');
const mongoose = require('mongoose');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const bluebird = require('bluebird');
const session = require('express-session');
const config = require('./config');
const routes = require('./routes');
const MongoDBStore = require('connect-mongodb-session')(session);
const Security = require('./lib/security');
const app = express();


const store = new MongoDBStore({
  uri: config.mongo.url,
  collection: config.mongo.sessions
});

app.disable('x-powered-by');


mongoose.Promise = bluebird;
mongoose.connect(config.mongo.url);

app.use(helmet());
app.use(session({
  secret: config.secret,
  resave: false,
  saveUninitialized: true,
  unset: 'destroy',
  store,
  name: `${config.name}-${Security.generateId()}`,
  genid: req => Security.generateId()
}));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use('/', routes);

app.listen(config.server.port, () => {
  console.log(`Magic happens on port ${config.server.port}`);
});

module.exports = app;

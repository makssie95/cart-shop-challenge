// const config = require('../config');


class Cart {
  static addToCart(product = null, qty = 1, cart) {
    if (!this.inCart(product.product_id, cart)) {
      const prod = {
        _id: product._id,
        name: product.name,
        value: product.value,
        qty,
        imageUrl: product.imageUrl,
        factor: product.factor
      };
      cart.items.push(prod);
      this.calculateTotals(cart);
    }
  }

  static removeFromCart(id = 0, cart) {
    for (let i = 0; i < cart.items.length; i += 1) {
      const item = cart.items[i];
      if (id === `${item._id}`) {
        cart.items.splice(i, 1);
        this.calculateTotals(cart);
      }
    }

  }

  static updateCart(ids = [], qtys = [], cart) {
    const map = [];
    let updated = false;
    ids.forEach((_id) => {
      qtys.forEach((qty) => {
        map.push({
          _id,
          qty
        });
      });
    });
    map.forEach((obj) => {
      cart.items.forEach((item) => {
        if (`${item._id}` === obj._id) {
          if (obj.qty > 0 && obj.qty !== item.qty) {
            item.qty = obj.qty;
            updated = true;
          }
        }
      });
    });
    if (updated) {
      this.calculateTotals(cart);
    }
  }

  static inCart(productID = 0, cart) {
    let found = false;
    cart.items.forEach((item) => {
      if (item.id === productID) {
        found = true;
      }
    });
    return found;
  }

  static calculateTotals(cart) {
    cart.totals = 0.00;
    cart.items.forEach((item) => {
      const value = item.value;
      const qty = item.qty;
      const amount = value * qty;
      cart.totals += amount;
    });
    const factorA = { amount: 0, value: 1, max: 5, maxAmount: 5 };
    const factorB = { amount: 0, value: 5, max: 15, maxAmount: 3 };
    const factorC = { amount: 0, value: 10, max: 30, maxAmount: 3 };
    const discount = cart.items.reduce((prev, item) => {
      let discountPrev = 0;
      if (item.factor === 'A' && factorA.amount < factorA.max) {
        factorA.amount += factorA.value * (item.qty > factorA.maxAmount ? factorA.max : item.qty);
        Cart._checkMaxAmount(factorA);
      } else if (item.factor === 'B' && factorB.amount < factorB.max) {
        factorB.amount += factorB.value * (item.qty > factorB.maxAmount  ? factorB.max : item.qty);
        Cart._checkMaxAmount(factorB);
      } else if (item.factor === 'C' && factorC.amount < factorC.max) {
        factorC.amount += factorC.value * (item.qty > factorC.maxAmount ? factorC.max : item.qty);
        Cart._checkMaxAmount(factorC);
      }
      discountPrev = factorA.amount + factorB.amount + factorC.amount;
      prev = discountPrev;
      if (prev > 30) {
        prev = 30;
        return prev;
      }
      return prev;
    }, 0);
    cart.discount = `${discount}%`;
    cart.totals *= (100 - discount) / 100;
  }

  static _checkMaxAmount(factor) {
    if (factor.amount > factor.max) {
      factor.amount = factor.max;
    }
  }

  static emptyCart(request) {

    if (request.session) {
      request.session.cart.items = [];
      request.session.cart.totals = 0.00;
      request.session.cart.formattedTotals = '';
    }


  }

}

// { _id: 5a6e5472c66e7004489f7de6,
//   name: 'Produto 3',
//   value: 500,
//   qty: 3,
//   imageUrl: 'src/teste.jpg',
//   factor: 'C' }

module.exports = Cart;
